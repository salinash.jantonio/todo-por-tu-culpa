var randomColor;
var total_size = 400;
var half_size = total_size/2;

function setup() {
	createCanvas(400, 400);
	background(255);
}

function draw() {
	randomColor = r_colour();
	stroke(randomColor);
	randomChord();
	randomChord();
}

function r_colour() {
	return color(random(128, 255), random(128, 255), random(128, 255));
}

function randomChord(){
	var angle1 = random(0, 2 * PI);
	var xpos1 = half_size + half_size * cos(angle1);
	var ypos1 = half_size + half_size * sin(angle1);
	
	var angle2 = random(0, 2 * PI);
	var xpos2 = half_size + half_size * cos(angle2);
	var ypos2 = half_size + half_size * sin(angle2);

	line(xpos1, ypos1, xpos2, ypos2);
}