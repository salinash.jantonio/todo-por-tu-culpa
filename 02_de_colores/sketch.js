function setup() {
	createCanvas(400, 400);
	background(0);
}

function draw() {
	fill(0, 20);
	rect(0, 0, width, height);
	noStroke();
	random_ellipse();
  	random_ellipse();
  	random_ellipse();
  	random_ellipse();
}

function random_ellipse() {
	var ellipse_color = color(random(155, 255), random(155, 255), random(155, 255));
	fill(ellipse_color);
	var w = random(0, width);
	var h = random(0, height);
	ellipse(w, h, 64, 64);
}