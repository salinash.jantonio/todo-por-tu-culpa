var yoff = 0.0;
var parte = false;
var col1 = 255;
var col2 = 127;

function setup() {
	createCanvas(400, 400);
}

function draw() {
	if (col1 == 255 || col1 == 127) {
		parte = !parte;
	}
	if (!parte) {
		col1 += 1;
		col2 += -1;
	}
	else {
		col1 += -1;
		col2 += 1;
	}

	background(col1, 255 , col2);
	fill(col2, 100, col1);
	beginShape();
	var xoff = 0; 

	for (var x = 0; x <= width; x += 10) {
		var y = map(noise(xoff, yoff), 0, 1, 200, 266);

		vertex(x, y);
    	// Increment x dimension for noise
    	xoff += 0.05;
    }
    // increment y dimension for noise
    yoff += 0.01;
    vertex(width, height);
    vertex(0, height);
    endShape(CLOSE);
}