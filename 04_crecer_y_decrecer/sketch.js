var radius = 0;
var radius_up = true;
var color_circle_up = true;
var color_back_up = true;
var color_circle = 0;
var color_back = 0;
var circle, back;

function setup() {
    createCanvas(400, 400);
    background(200);
    noStroke();
}

function draw() {
    // Cambio radio circulo
    if (radius_up) {
        if (radius == height) {
            radius_up = false;
        }
        else {
            radius += 1;
        }
    }
    else {
        if (radius == 0) {
            radius_up = true;
        }
        else {
            radius += -1;
        }
    }
    // Cambio Color Circulo
    if (color_circle_up) {
        if (color_circle == 255) {
            color_circle_up = false;
        }
        else {
            color_circle += 1;
        }
    }
    else {
        if (color_circle == 0) {
            color_circle_up = true;
        }
        else {
            color_circle += -1;
        }
    }
    // Cambio Color Back
    if (color_back_up) {
        if (color_back == 255) {
            color_back_up = false;
        }
        else {
            color_back += 1;
        }
    }
    else {
        if (color_back == 0) {
            color_back_up = true;
        }
        else {
            color_back += -1;
        }
    }
    back = color(color_back, random(127, 255), 0);
    background(back);
    circle = color(random(127, 255), 0, color_circle);
    fill(circle);
    ellipse(width/2, height/2, radius, radius);
}