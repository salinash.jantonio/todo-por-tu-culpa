var xspacing = 10.67;   // How far apart should each horizontal location be spaced
var w;              // Width of entire wave

var theta = 0.0;  
var amplitude = 166.77;  
var period = 333.34;  
var dx;  
var yvalues_1;  
var yvalues_2; 
var yvalues_3;  
var yvalues_4; 

function setup() {
	createCanvas(400, 400);
	w = width+10.67;
	dx = (TWO_PI / period) * xspacing;
	yvalues_1 = new Array(parseInt(w/xspacing));
	yvalues_2 = new Array(parseInt(w/xspacing));
	yvalues_3 = new Array(parseInt(w/xspacing));
	yvalues_4 = new Array(parseInt(w/xspacing));
}

function draw() {
	background(0);
	calcWave();
	renderWave();
}

function calcWave() {
	// Increment theta (try different values for 'angular velocity' here
	theta += 0.06;

	// For every x value, calculate a y value with sine function
	var x = theta;
	for (var i = 0; i < yvalues_1.length; i++) {
		yvalues_1[i] = sin(x)*amplitude;
		yvalues_2[i] = sin(x)*amplitude*(-1);
		yvalues_3[i] = cos(x)*amplitude;
		yvalues_4[i] = cos(x)*amplitude*(-1);
		x+=dx;
	}
}

function renderWave() {
	noStroke();
	// A simple way to draw the wave with an ellipse at each location
	for (var x = 0; x < yvalues_1.length; x++) {
		fill("#eaebe1");
		ellipse(x*xspacing, height/2+yvalues_1[x], 10.67, 10.67);
		fill("#ff647e");
		ellipse(x*xspacing, height/2+yvalues_2[x], 10.67, 10.67);
		fill("#f9f575");
		ellipse(x*xspacing, height/2+yvalues_3[x], 10.67, 10.67);
		fill("#60d5d0");
		ellipse(x*xspacing, height/2+yvalues_4[x], 10.67, 10.67);
	}
}