var e1, e2;
var colores;
var x_e, y_e;
var derecha;
var nu_color;

function setup() {
    createCanvas(400, 400);
    noStroke();
    e1 = new Eye( int(width*0.25),  int(width*0.5), 80);
    e2 = new Eye( int(width*0.75),  int(width*0.5), 80);  
    colores = new Array(2);
    y_e = 400;
    x_e = 0;
    derecha = true;
    colores[0] = color(255, 99, 125);
    colores[1] = color(102, 215, 209);
    nu_color = 0;
}

function draw() {
    background(colores[nu_color % 2]);
    e1.update(x_e, y_e);
    e2.update(x_e + 200, y_e);
    e1.display();
    e2.display();
    if (derecha) {
        x_e += 2;
        if (x_e >= 400) {
            derecha = false;
        }
    }
    else {
        x_e += -2;
        if (x_e <= 0) {
            derecha = true;
        }
    }
    if(x_e % 75 == 0) {
        nu_color += 1;
    }
}

class Eye {
    constructor(tx, ty, ts) {
        this.x = tx;
        this.y = ty;
        this.size = ts;
        this.angle = 0.0;
    }

    update(mx, my) {
        this.angle = atan2(my-this.y, mx-this.x);
    }

    display() {
        push();
        translate(this.x, this.y);
        fill(255);
        ellipse(0, 0, this.size, this.size);
        rotate(this.angle);
        fill(0);
        ellipse(this.size/4, 0, this.size/2, this.size/2);
        pop();
    }
}