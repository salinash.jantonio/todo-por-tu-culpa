var from, to, c1, c2;

function setup() {
  createCanvas(400, 400);
  background(255);
  noStroke();
}

function draw() {
  background(0);
  c1 = color(127, 181, 181, 0.2 * 255);
  c2 = color(243, 123, 173, 0.2 * 255);
  for (var i = 0; i < 10; i++) {
    fill(c1);
    quad(
      random(-20, 420), random(-20, 420),
      random(-20, 420), random(-20, 420),
      random(-20, 420), random(-20, 420),
      random(-20, 420), random(-20, 420)
    );
    fill(c2);
    quad(
      random(-20, 420), random(-20, 420),
      random(-20, 420), random(-20, 420),
      random(-20, 420), random(-20, 420),
      random(-20, 420), random(-20, 420)
    );
  }
  frameRate(10);
}