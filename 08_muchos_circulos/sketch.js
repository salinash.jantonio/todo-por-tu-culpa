function setup() {
	createCanvas(400, 400);
}

function draw() {
	background(255, 105, 97);
	noStroke();
	var ran = random(20, 70);
	fill(119, 255, 119);
	for(var i=0; i<4; i++) {
		for(var j=0; j<4; j++) {
			ellipse(80+(i*80), 80+(j*80), ran, ran); 
		}
	}
}