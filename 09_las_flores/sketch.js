var c;
var up;
var x, y;

function setup() {
	createCanvas(400, 400);
	c = 0;
	up = true;
	x = new Array(16);
	y = new Array(16);	
	for (var i=0; i<16; i++) {
		x[i] = int(random(-20, 420));
		y[i] = int(random(-20, 420));
	}
}

function draw() {
	background(0);
	var f1 = map(c, 0, 150, 0, 204);
	var color_f1 = color(f1, 101, 192, 127);
	var f2 = map(c, 0, 150, 0, 224);
	var color_f2 = color(101, f2, 192, 127);
	var f3 = map(c, 0, 150, 0, 204);
	var color_f3 = color(192, 101, f3, 127);
	var f4 = map(c, 0, 150, 0, 224);
	var color_f4 = color(101, f4, f4, 127);
	// A design for a simple flower
	for(var i=0; i<16; i++) {
		if (0<=i && i<4) {
			fill(color_f1);
		}
		else if(4<=i && i<8) {
			fill(color_f2);
		}
		else if(8<=i && i<12) {
			fill(color_f3);
		}
		else {
			fill(color_f4);
		}
		gen_flor(x[i], y[i]);
	}
	if (up == true) {
		c += 1;
		if (c == 150) {
			up = false;
		}
	}
	else {
		c -= 1;
		if (c == 0) {
			up = true;
		}
	}
}

 function gen_flor(x, y) {
   // A design for a simple flower
   push();
   translate(x, y);
   noStroke();
   for (var i = 0; i < 10; i ++) {
     ellipse(0, 20, 20, 107);
     rotate(PI/5);
   }
   pop();
}