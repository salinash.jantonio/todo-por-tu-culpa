let font_main;
var Y_AXIS = 1;
var X_AXIS = 2;
var c1, c2;
var a, b, cont;
var up;

function setup() {
	font_main = loadFont("data/Montserrat-Bold.ttf", 80);
	cont = 0;
	createCanvas(400, 400);
	c1 = color(204, 102, 0);
	c2 = color(0, 102, 204);
	up = true;
	textFont(font_main);
	textAlign(CENTER, CENTER);
}

function draw() {
	a = int(map(cont, 0, 100, 104, 204));
	b = int(map(cont, 0, 100, 153, 53));
	c1 = color(a, 102, b);
	c2 = color(b, 202, a);
	setGradient(0, 0, width, height, c1, c2, X_AXIS);
	if (up) {
		cont++;
		if(cont == 100) {
			up = false;
		}
	}
	else {
		cont--;
		if(cont == 0) {
			up = true;
		}
	}
	fill(255);
	text("todo\npor tu\nculpa", width/2, height/2);
}

function setGradient(x, y, w, h, c1, c2, axis) {
	if (axis == Y_AXIS) {
		for (var i = y; i <= y + h ; i++) {
			var inter = map(i, y, y + h, 0, 1);
			var c = lerpColor(c1, c2, inter);
			stroke(c);
			line(x, i, x + w, i);
		}
	}
	else if (axis == X_AXIS) {
		for (var i = x; i <= x + w; i++) {
			var inter = map(i, x, x + w, 0, 1);
			var c = lerpColor(c1, c2, inter);
			stroke(c);
			line(i, y, i, y + h);
		}
	}
}